var DateFormats = { 
	short: "DD MMMM - YYYY", 
	long: "dddd DD.MM.YYYY HH:mm" 
}; 
 
Template7.registerHelper('formatDate', function (date, time, format){ 
	var datetime = date + " " + time;
	if (moment) { 
		// can use other formats like 'lll' too 
		format = DateFormats[format] || format; 
		return moment(datetime).format(format); 
	} else { 
		console.log('Moment.js not installed. ');
		return datetime; 
	} 
}); 

// Initialize app
var myApp = new Framework7({
	precompileTemplates: true,
});

// If we need to use custom DOM library, let's save it to $$ variable:
var $$ = Dom7;

// Fix URL encoding issue
// https://stackoverflow.com/questions/39097721/how-fix-or-disable-proxy-in-phonegap
/*(function() {
    var xhr = {};
    xhr.open = XMLHttpRequest.prototype.open;

    XMLHttpRequest.prototype.open = function(method, url) {
        console.log(url);
        if(url.indexOf('/proxy/') == 0){
            url = window.decodeURIComponent(url.substr(7));
        }
        xhr.open.apply(this, arguments);
    };
})(window);*/

// Open target="_blank" links with InnAppBrowser
window.addEventListener('load', function () {    
    $$(document).on('click', 'a[target="_system"],a[target="_blank"]', function (e) {
		e.preventDefault();
		var url = this.href;
		window.open(url,"_system");                    
    });
    //}
}, false);

var WpRest = 'http://www.peopleperformance.com.au/wp-json/wp/v2/';

// Add view
var mainView = myApp.addView('.view-main',
{
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true
});

// Handle Cordova Device Ready Event
$$(document).on('deviceready', function()
{
    //console.log("Device is ready!");

});


// Now we need to run the code that will be executed only for About page.

// Option 1. Using page callback for page (for "about" page in this case) (recommended way):
myApp.onPageInit('about', function (page)
{
   myApp.alert('Here comes About page');

})
myApp.onPageInit('list', function (page)
{
	myApp.showProgressbar( $$('.content-block') );
	
    $$.ajax({
		url: WpRest + 'events-recurring/',
		contentType: "application/json",
		dataType: 'json',
		crossDomain: true,
		data:
		{
			'order': 'desc',
			'orderby': 'id',
		},
		success: function(json, status, xhr) {
			
			if( status == 200 ) {

				console.log(json);
				var courseListHTML = Template7.templates.courseListTemplate({
					posts: json
				});
				$$('#ajaxList').append( courseListHTML );
				
				myApp.hideProgressbar( $$('.content-block') );

			}
			else
			{
				console.log(xhr.status);
				console.log(xhr.response);
				console.log(xhr.responseText)
				console.log(xhr.statusText);
				
				myApp.hideProgressbar( $$('.content-block') );
				myApp.showProgressbar( $$('.content-block'), 50, 'red' );
			}

		},
		error: function(jqXHR, status, error) {
			console.log('error ' + jqXHR.statusText);
			
			myApp.hideProgressbar( $$('.content-block') );
			myApp.showProgressbar( $$('.content-block'), 50, 'red' );
		}
	});
})
myApp.onPageInit('course-details', function (page)
{
	//console.log( 'loading event details' );
	
	// TODO: Load list of recurrences (/events/). Need to add recurrence id filter to endpoint.
	// Try this code https://github.com/airesvsg/acf-to-rest-api/issues/51
	
	// Fetch Recurring Event details
	$$.ajax({
		url: WpRest + 'events-recurring/' + page.query.id,
		contentType: "application/json",
		dataType: 'json',
		crossDomain: true,
		data:
		{

		},
		success: function(json, status, xhr) {

			if( status == 200 ) {

				console.log(json);
				var courseDetailsHTML = Template7.templates.courseDetailsTemplate({
					post: json
				});
				$$('#page-content').append( courseDetailsHTML );

			}
			else
			{
				console.log(xhr.status);
				console.log(xhr.response);
				console.log(xhr.responseText)
				console.log(xhr.statusText);
			}

		},
		error: function(jqXHR, status, error) {
			console.log('error ' + jqXHR.statusText);
		}
	});
	
	// Fetch Recurrences
	$$.ajax({
		url: WpRest + 'events/',
		contentType: "application/json",
		dataType: 'json',
		crossDomain: true,
		data:
		{
			filter: {
				'recurrence' : page.query.event_id, 
				'scope' : '3-months'
			}
		},
		success: function(json, status, xhr) {

			if( status == 200 ) {

				console.log(json);
				var courseDatesHTML = Template7.templates.courseDatesTemplate({
					events: json
				});
				$$('#page-content').append( courseDatesHTML );

			}
			else
			{
				console.log(xhr.status);
				console.log(xhr.response);
				console.log(xhr.responseText)
				console.log(xhr.statusText);
			}

		},
		error: function(jqXHR, status, error) {
			console.log('error ' + jqXHR.statusText);
		}
	});
})

myApp.onPageInit('course-booking', function (page)
{
	
	$$('#form-content').append( '<iframe src="http://peopleperformance.com.au/booking-form/?event_id=' + page.query.event_id + '"></iframe>' );

})